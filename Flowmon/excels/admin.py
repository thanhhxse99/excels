from django.contrib import admin
from excels.models import so_unit
from excels.models import Genre
# Register your models here.
admin.site.register(so_unit)
admin.site.register(Genre)