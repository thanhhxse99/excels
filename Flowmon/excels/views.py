from django.shortcuts import render

# Create your views here.
from excels.models import so_unit, Genre
from django.views import generic
from django.http import JsonResponse
import openpyxl
import os
from django.core.exceptions import ValidationError
from django.core.files.storage import FileSystemStorage
import csv


def index(request):
    """View function for home page of site."""
    # Generate counts of some of the main objects
    num_user = so_unit.objects.all().count()
    context = {
        'num_user': num_user,
    }
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)

# This function allows to read and display uploaded excel files by customers
# def import_data(request):
#     if "GET" == request.method:
#         return render(request, 'index.html', {})
#     else:
#         excel_file = request.FILES["excel_file"]
#         # where to check extension or file size
#         if(excel_file.size < 10485760):
       
#             wb = openpyxl.load_workbook(excel_file)

#             # getting a particular sheet by name out of many sheets
#             worksheet = wb["Sheet1"]
#             print(worksheet)
#             idx = 0
#             excel_data = { }
#             # iterating over the rows and getting value from each cell in row
#             for row in worksheet.iter_rows():
#                 row_data = list()
#                 for cell in row:
#                     row_data.append(str(cell.value))
#                 # excel_data.append(row_data)
#                 excel_data.update({idx:row_data})
#                 idx += 1
#             return JsonResponse(excel_data, safe = False)
#         else:
#             raise ValidationError("The maximum file size that can be uploaded is 10MB")
#             # return render(request, 'index.html', {"excel_data":excel_data})


# --- Export Json Files ---
def import_data(request):
    if "GET" == request.method:
        return render(request, 'index.html', {})
    else:
        excel_file = request.FILES["excel_file"]
        
        fs = FileSystemStorage()
        # save file to local filesystem. 
        # https://docs.djangoproject.com/en/2.2/ref/files/storage/
        excel_file = fs.save(excel_file.name, excel_file)
        
        
        statinfo = os.stat(excel_file)
        file_size = statinfo.st_size
        
        if(file_size < 5000000):
            uploaded_file_url = fs.url(excel_file)
            # fields = []
            # rows = []
            excel_data = { }
            idx = 0 
            # reading csv file
            with open(uploaded_file_url, 'r') as csvfile:
                # create a csv reader object
                csvreader = csv.reader(csvfile, delimiter = ",")
                # extracting field name through first row
                # fields = csvreader.next()
                # extracting each data row one by one and covert list to dictionary type
                for row in csvreader:
                    # rows.append(row)
                    excel_data.update({idx:row})
                    idx += 1
                '''
                 delete local filesystem or fs.save() will create a copy of uploaded file
                 if uploaded file is not unique. (reupload)
                '''
                fs.delete(excel_file)
            return JsonResponse(excel_data, safe = False)
            
        else:
            fs.delete(excel_file)
            raise ValidationError("The maximum file size that can be uploaded is 5MB")
    

        