from django.db import models
from django.urls import reverse 
# Create your models here.
class Genre(models.Model):
    """Model representing a genre """
    name = models.CharField(
        max_length=200,
        help_text="Enter a genre "
        )

    def __str__(self):

        return self.name


class so_unit(models.Model):
    user = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    types = models.ManyToManyField(Genre, help_text="what is this SO unit type ?")


